Given some problems between the default instalation of sharp and docker or some OS, is needed to do once done the npm install on the task_service directory:

```
npm r sharp
npm install --arch=x64 --platform=linux sharp
```

The lambda function with node has the same problem and with the documentation (https://sharp.pixelplumbing.com/install#aws-lambda) can't fix the issue.
