CREATE TABLE IF NOT EXISTS tasks (
  id SERIAL PRIMARY KEY,
  resource_path VARCHAR(255) NOT NULL,
  creation_timestamp TIMESTAMP DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS tasks_status (
  id SERIAL PRIMARY KEY,
  task_id INTEGER REFERENCES tasks(id),
  task_status VARCHAR(20) NOT NULL,
  creation_timestamp TIMESTAMP DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS images (
  image_id SERIAL PRIMARY KEY,
  md5_hash VARCHAR(32) NOT NULL,
  resolution VARCHAR(20) NOT NULL,
  binary_path VARCHAR(255) NOT NULL,
  is_original BOOLEAN NOT NULL,
  creation_timestamp TIMESTAMP DEFAULT NOW(),
  task_id INTEGER REFERENCES tasks(id)
);