import io
import base64
from PIL import Image
import hashlib

def process(event, context):
    try:
        # Decode the base64 buffer to bytes
        original_image_filename = event["originalImageFilename"]
        original_image_buffer = event["originalImageBuffer"]

        image_bytes = base64.b64decode(original_image_buffer)

        # Open the image with PIL
        image_pil = Image.open(io.BytesIO(image_bytes))

        # Resize the image to a width of 1024 and maintain the aspect ratio
        ratio_1024 = 1024 / image_pil.width
        height_1024 = int(image_pil.height * ratio_1024)
        resized_image_1024 = image_pil.resize((1024, height_1024))

        # Resize the image to a width of 800 and maintain the aspect ratio
        ratio_800 = 800 / image_pil.width
        height_800 = int(image_pil.height * ratio_800)
        resized_image_800 = image_pil.resize((800, height_800))

        # Get the metadata of the original image
        original_width = image_pil.width
        original_height = image_pil.height
        md5_hash_original = hashlib.md5(image_bytes).hexdigest()

        # Convert the resized images to bytes
        buffer_1024 = io.BytesIO()
        resized_image_1024.save(buffer_1024, format=image_pil.format)
        buffer_1024.seek(0)
        image_bytes_1024 = buffer_1024.read()

        buffer_800 = io.BytesIO()
        resized_image_800.save(buffer_800, format=image_pil.format)
        buffer_800.seek(0)
        image_bytes_800 = buffer_800.read()

        # Get the metadata of the resized images
        width_1024 = 1024
        height_1024 = int(height_1024)
        md5_hash_1024 = hashlib.md5(image_bytes_1024).hexdigest()

        width_800 = 800
        height_800 = int(height_800)
        md5_hash_800 = hashlib.md5(image_bytes_800).hexdigest()

        # Get the file name and extension
        name, extension = original_image_filename.rsplit('.', 1)

        # Create result objects for each resized image
        original_result = {
            'width': original_width,
            'height': original_height,
            'md5Hash': md5_hash_original,
            'name': name,
            'ext': extension,
            'isOriginal': True,
            # 'buffer': base64.b64encode(image_bytes).decode('utf-8')
        }

        resized_1024_result = {
            'width': width_1024,
            'height': height_1024,
            'md5Hash': md5_hash_1024,
            'name': name,
            'ext': extension,
            'isOriginal': False,
            'buffer': base64.b64encode(image_bytes_1024).decode('utf-8')
        }

        resized_800_result = {
            'width': width_800,
            'height': height_800,
            'md5Hash': md5_hash_800,
            'name': name,
            'ext': extension,
            'isOriginal': False,
            'buffer': base64.b64encode(image_bytes_800).decode('utf-8')
        }

        # Create a dictionary with the names and resized images in base64
        result = {
            'originalImageData': original_result,
            'resized1024ImageData': resized_1024_result,
            'resized800ImageData': resized_800_result
        }

        # Return the result dictionary
        return result
    except Exception as error:
        raise ValueError(str(error))
