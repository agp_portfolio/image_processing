import path from "path";
import fs from "fs";
import ImageRepository from "../repositories/images";

const processImageCallback = async (
  originalBinaryPath: string,
  images: any,
  taskId: number
) => {
  let absolute_path = process.env.DIRECTORIES_BASE_PATH ? path.join(__dirname, process.env.DIRECTORIES_BASE_PATH) : ""
  const resized1024DirPath = path.join(absolute_path, `/output/${images.originalImageData.name}/1024`);
  console.log(resized1024DirPath)
  const resized800DirPath = path.join(absolute_path, `/output/${images.originalImageData.name}/800`);

  await ImageRepository.addImage(
    images.originalImageData.md5Hash,
    images.originalImageData.width,
    images.originalImageData.heigth,
    originalBinaryPath,
    true,
    taskId
  );
  if (!fs.existsSync(resized1024DirPath)) {
    fs.mkdirSync(resized1024DirPath, { recursive: true });
  }
  if (!fs.existsSync(resized800DirPath)) {
    fs.mkdirSync(resized800DirPath, { recursive: true });
  }
  const resized1024BinaryPath = path.join(
    resized1024DirPath,
    `${images.resized1024ImageData.md5Hash}.${images.resized1024ImageData.ext}`
  );
  fs.writeFileSync(
    resized1024BinaryPath,
    Buffer.from(images.resized1024ImageData.buffer, "base64")
  );
  await ImageRepository.addImage(
    images.resized1024ImageData.md5Hash,
    images.resized1024ImageData.width,
    images.resized1024ImageData.heigth,
    resized1024BinaryPath,
    false,
    taskId
  );
  const resized800BinaryPath = path.join(
    resized800DirPath,
    `${images.resized800ImageData.md5Hash}.${images.resized800ImageData.ext}`
  );

  fs.writeFileSync(
    resized800BinaryPath,
    Buffer.from(images.resized800ImageData.buffer, "base64")
  );
  await ImageRepository.addImage(
    images.resized800ImageData.md5Hash,
    images.resized800ImageData.width,
    images.resized800ImageData.heigth,
    resized800BinaryPath,
    false,
    taskId
  );
};

export default processImageCallback;
