import logger from "../logger";

const AWS = require("aws-sdk");

AWS.config.update({accessKeyId: process.env.AWS_KEY_ID, secretAccessKey: process.env.AWS_SECRET});

AWS.config.update({ region: "eu-west-1" });

const lambda = new AWS.Lambda();

const invokeProcessImageLambda = async (
  originalImageBuffer: string,
  originalImageFilename: string
) => {
  const params = {
    FunctionName: process.env.LAMBDA_FUNCTION,
    Payload: JSON.stringify({
      originalImageBuffer: originalImageBuffer,
      originalImageFilename: originalImageFilename,
    }),
  };

  try {
    const result = await lambda.invoke(params).promise();
    const response = JSON.parse(result.Payload);
    return response;
  } catch (error) {
    logger.error("Error al invocar la función Lambda:", error);
    throw error;
  }
};

export default invokeProcessImageLambda;
