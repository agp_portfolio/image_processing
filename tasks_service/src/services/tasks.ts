import TaskRepository from "../repositories/tasks";
import { TaskPayload } from "../models/tasks";
import { ParamsDictionary } from "express-serve-static-core";
import logger from "../logger";
import fs from "fs";
import path from "path";
import invokeProcessImageLambda from "../utils/processImages";
import processImageCallback from "../utils/saveImages";

const getTask = async (params: ParamsDictionary) => {
  const id: number = parseInt(params.id);

  const results = await TaskRepository.getTask(id);

  return results;
};

const addTask = async (body: TaskPayload) => {
  const resourcePath: string = body.resourcePath;

  const results: { id: number } = await TaskRepository.addTask(resourcePath);

  await TaskRepository.addStatus(results.id, "starting");

  await TaskRepository.addStatus(results.id, "processing");
  const originalBinaryPath = resourcePath;
  let absolute_path = process.env.DIRECTORIES_BASE_PATH ? path.join(__dirname, process.env.DIRECTORIES_BASE_PATH) : ""
  const originalImageBuffer = fs.readFileSync(path.join(absolute_path, originalBinaryPath)).toString('base64');
  const imageFilename = path.basename(originalBinaryPath);
  invokeProcessImageLambda(originalImageBuffer, imageFilename)
    .then(async (images) => {
      await processImageCallback(originalBinaryPath, images, results.id);
      TaskRepository.addStatus(results.id, "finished");
      logger.info(`Task ${results.id} finished`);
    })
    .catch((error) => {
      TaskRepository.addStatus(results.id, "error");
      logger.error(error);
    });

  return results;
};

export default {
  getTask,
  addTask,
};
