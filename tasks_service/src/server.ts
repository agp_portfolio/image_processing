import app from "./app";
import logger from "./logger";

// Start server
app.listen(app.get("port"), async () => {
  logger.info(`tasks_service running on port ${app.get("port")}.`);
});

export default app;
