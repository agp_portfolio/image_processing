import { QueryResult } from "pg";

import { pool } from "../database";

const addImage = async (
  md5: string,
  width: number,
  height: number,
  binaryPath: string,
  isOriginal: boolean,
  taskId: number
) => {
  const client = await pool.connect();
  try {
    const resolution = `${width}x${height}`;
    const query = `INSERT INTO images (md5_hash, resolution, binary_path, is_original, task_id)
    VALUES ($1, $2, $3, $4, $5)`;

    await client.query(query, [
      md5,
      resolution,
      binaryPath,
      isOriginal,
      taskId,
    ]);
  } finally {
    client.release();
  }
};

export default {
  addImage,
};
