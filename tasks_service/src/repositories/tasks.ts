import { QueryResult } from "pg";

import { pool } from "../database";

const getTask = async (id: number) => {
  const client = await pool.connect();
  try {
    const query = `SELECT task_status FROM tasks_status
      WHERE task_id = $1
      ORDER BY creation_timestamp DESC
      LIMIT 1`;

    const results: QueryResult = await client.query(query, [id]);

    return results.rows[0];
  } finally {
    client.release();
  }
};

const addTask = async (path: string) => {
  const client = await pool.connect();
  try {
    const query = `INSERT INTO tasks (resource_path)
    VALUES ($1)
    RETURNING id`;

    const results: QueryResult = await client.query(query, [path]);

    return results.rows[0];
  } finally {
    client.release();
  }
};

const addStatus = async (id: number, status: string) => {
  const client = await pool.connect();
  try {
    const query = `INSERT INTO tasks_status (task_id, task_status)
    VALUES ($1, $2)`;

    await client.query(query, [id, status]);

    return;
  } finally {
    client.release();
  }
};

export default {
  getTask,
  addTask,
  addStatus,
};
