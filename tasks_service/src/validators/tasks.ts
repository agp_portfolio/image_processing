import { Request, Response, NextFunction } from "express";
import fs from "fs";
import Joi, { ValidationResult } from "joi";
import path from "path";
import logger from "../logger";

const validateQueryParamsId = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { params }: { params: any } = req;

  // Needed an object with id property
  const schema = Joi.object()
    .keys({
      id: Joi.number().required(),
    })
    .required();

  const result: ValidationResult = schema.validate(params);

  const { error } = result;

  const valid: boolean = error == null;

  if (!valid) {
    res.status(400).json({
      message: "Invalid request",
    });
  } else {
    next();
  }
};

const validateBody = (req: Request, res: Response, next: NextFunction) => {
  const { body }: { body: any } = req;

  // Needed an object with id property
  const schema = Joi.object()
    .keys({
      resourcePath: Joi.string().required(),
    })
    .required();

  const result: ValidationResult = schema.validate(body);

  const { error } = result;

  let valid: boolean = error == null;

  let absolute_path = process.env.DIRECTORIES_BASE_PATH ? path.join(__dirname, process.env.DIRECTORIES_BASE_PATH) : ""
  if (!fs.existsSync(path.join(absolute_path, body.resourcePath))) {
    valid = false;
    logger.info("Image not found on the route " + path.join(absolute_path, body.resourcePath))
  }


  if (!valid) {
    res.status(400).json({
      message: "Invalid request",
    });
  } else {
    next();
  }
};

export default {
  validateQueryParamsId,
  validateBody,
};
