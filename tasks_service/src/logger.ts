const { createLogger, transports, format } = require("winston");

const logger = createLogger({
  level: "info",
  defaultMeta: { service: "task_service" },
  transports: [
    new transports.Console(),
    new transports.File({ filename: "../logs/task_service.log" }),
  ],
  format: format.combine(
    format.errors({ stack: true }), // This shows the error message
    format.printf(({ level, message, service }: { level: string, message: string, service: string }) => {
      return `${level}: [${service}] ${message}`;
    })
  ),
});

export default logger;
