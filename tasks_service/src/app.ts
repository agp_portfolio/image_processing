// Email Service: microservice implementing email notifications.

import express, { Express } from "express";
import cors from "cors";
import morgan from "morgan";

import swaggerUI from "swagger-ui-express";
import swaggerJsDoc from "swagger-jsdoc";
import { options } from "./swaggerOptions";

import bodyParser from "body-parser";
import health from "./interceptors/health";

require("dotenv").config();

const app: Express = express();

app.set(
  "port",
  process.env.NODE_ENV === "production" ? 3000 : process.env.PORT || 3001
);

// Middlewares
app.use(cors());
app.use(morgan("dev"));
app.use(bodyParser.json());
require("./logger");

// Routes
app.use(health);
app.use("/", require("./routes/tasks.routes"));

// Swagger
const specs = swaggerJsDoc(options);
app.use("/docs", swaggerUI.serve, swaggerUI.setup(specs));

export default app;
