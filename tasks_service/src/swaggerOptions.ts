export const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Task microservice",
      version: "1.0.0",
      description: "The task microservice for the image_processing service",
    },
  },
  servers: [
    {
      url: "http://localhost:3000",
    },
  ],
  apis: ["./src/routes/*.ts", "./src/models/*.ts"],
};
