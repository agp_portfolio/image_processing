import request from "supertest";
import { Response } from "supertest";
import app from "../../../app";
import path from "path";

describe("### GET /tasks/${id}", () => {
  test("should return 200", async () => {
    const resourcePath: string = path.join(
      __dirname,
      "../../../../..",
      "resources/image.jpg"
    );

    const resCreate: Response = await request(app).post(`/tasks`).send({
      resourcePath,
    });

    const res: Response = await request(app).get(`/tasks/${resCreate.body.id}`);

    expect(res.statusCode).toEqual(200);
  });

  test("should return 400", async () => {
    const res: Response = await request(app).get(`/tasks/a`);

    expect(res.statusCode).toEqual(400);
  });
});

describe("### POST /tasks", () => {
  test("should return 201", async () => {
    const resourcePath: string = path.join(
      __dirname,
      "../../../../..",
      "resources/image.jpg"
    );

    const res: Response = await request(app).post(`/tasks`).send({
      resourcePath,
    });

    expect(res.statusCode).toEqual(201);
    expect(res.body).toHaveProperty("id");
  });

  test("should return 400", async () => {
    const resourcePath: string = "resources/no_image.jpg";

    const res: Response = await request(app).post(`/tasks`).send({
      resourcePath,
    });

    expect(res.statusCode).toEqual(400);
  });
});
