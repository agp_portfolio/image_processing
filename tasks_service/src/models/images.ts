export interface Image {
  width: number;
  heigth: number;
  md5Hash: string;
  binaryPath?: string;
  isOriginal: boolean;
  buffer?: Buffer;
}
