import { NextFunction, Request, Response } from "express";

const health = async (req: Request, res: Response, next: NextFunction) => {
  if (req.url == "/health" && req.method == "GET") {
    return res.status(200).json("The server is up and running");
  }

  next();
};

export default health;
