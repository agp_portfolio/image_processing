import { Pool, PoolConfig } from "pg";
import logger from "./logger";

const PgPool = require("pg").Pool;

var config: PoolConfig = {
  user:
    process.env.NODE_ENV === "test"
      ? process.env.TESTS_DATABASE_INFO_USER
      : process.env.DATABASE_INFO_USER || "postgres",
  host:
    process.env.NODE_ENV === "test"
      ? process.env.TESTS_DATABASE_INFO_HOST
      : process.env.NODE_ENV === "production"
      ? "postgres"
      : process.env.DATABASE_INFO_HOST || "localhost",
  database:
    process.env.NODE_ENV === "test"
      ? process.env.TESTS_DATABASE_INFO_DATABASE
      : process.env.DATABASE_INFO_DATABASE || "tasks",
  password:
    process.env.NODE_ENV === "test"
      ? process.env.TESTS_DATABASE_INFO_PASSWORD
      : process.env.DATABASE_INFO_PASSWORD || "postgres",
  port:
    process.env.NODE_ENV === "test"
      ? Number(process.env.TESTS_DATABASE_INFO_PORT)
      : Number(process.env.DATABASE_INFO_PORT) || 5432,
};

export const pool: Pool = new PgPool(config);

if (process.env.NODE_ENV !== "test") {
  const client = pool.connect(function (err) {
    if (err) {
      logger.error("error connecting to the database: " + err.stack);
    }
    logger.info("db connection authenticated");
  });
}

module.exports = {
  pool,
};
