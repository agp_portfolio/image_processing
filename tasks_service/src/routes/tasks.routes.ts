import express, { Router } from "express";
import controller from "../controllers/tasks";
import validators from "../validators/tasks";

const router: Router = express.Router();

/**
 * @swagger
 * /tasks/{id}:
 *  get:
 *    summary: return a specific task
 *    tags: [Task]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *        description: the task id
 *        example: 1
 *    responses:
 *      200:
 *        description: specific suscription
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                task_status:
 *                  type: string
 *              example:
 *                task_status: processing
 *      404:
 *        description: not found
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *              example:
 *                message: Task not found
 *      500:
 *        description: server error
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *              example:
 *                message: Server error
 */
router.get("/tasks/:id", validators.validateQueryParamsId, controller.getTask);

/**
 * @swagger
 * /tasks:
 *  post:
 *    summary: create new task
 *    tags: [Task]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *            schema:
 *              type: object
 *              properties:
 *                resourcePath:
 *                  type: string
 *              example:
 *                resourcePath: /resources/image.jpg
 *    responses:
 *      201:
 *        description: the new suscription
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                id:
 *                  type: integer
 *            example:
 *              id: 1
 *      400:
 *        description: bad request
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *              example:
 *                message: Bad request
 *      500:
 *        description: server error
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *              example:
 *                message: Server error
 */
router.post("/tasks", validators.validateBody, controller.addTask);

export = router;
