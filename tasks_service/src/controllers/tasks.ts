import { Request, Response, NextFunction } from "express";
import logger from "../logger";
import TaskServices from "../services/tasks";

const getTask = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const results = await TaskServices.getTask(req.params);

    res.status(200).json(results);
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      message: "Server error",
    });
  }
};

const addTask = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const results = await TaskServices.addTask(req.body);

    res.status(201).json(results);
  } catch (error) {
    return res.status(500).json({
      message: "Server error",
    });
  }
};

export default {
  getTask,
  addTask,
};
