# Image processing Service

This application is designed to resize images using a microservice and a lambda function. It is built using Node.js and Typescript. Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine, used to create efficient and fast web applications. Typescript is a typed superset of JavaScript that compiles to plain JavaScript. Lambda is a serverless function provided by AWS.

The following frameworks and libraries are used in this project:

* **Aws-Sdk**: The AWS SDK is a powerful set of tools and libraries offered by Amazon Web Services. It enables developers to easily integrate and utilize AWS services in their applications, unlocking the full potential of cloud computing for building scalable and robust solutions.
* **Body-Parser**: Used for parsing incoming request bodies. This library reads the data from a request and turns it into an easily manageable JSON object.
* **Cors**: Used for enabling Cross-Origin Resource Sharing (CORS). This library allows a web server to receive requests from external domains, allowing developers to create distributed web applications.
* **Dotenv**: Used for loading environment variables from a .env file. This library reads a .env file and loads the environment variable values into the application. This is useful for development and testing, as it allows values to be kept out of the source code.
* **Express**: Used for creating a web server. This open-source web application framework is a minimalistic framework for web and API applications that provides a wide range of features such as support for multiple internet protocols, routing, authentication, database support, etc.
* **Fs**: A built-in Node.js module used for interacting with the file system. It provides functions for reading, writing, and manipulating files and directories.
* **Joi**: Used for schema validation. This library is a schema validation tool that allows developers to validate the structure and data of an object. This ensures that the data is valid before being processed.
* **Morgan**: Used for logging HTTP requests. This library is an HTTP request logging tool that offers a variety of ways to log and display information about incoming requests.
* **PG**: Used for connecting to a PostgreSQL database. This library provides an Application Programming Interface (API) for connecting to the PostgreSQL database and executing SQL queries.
* **Swagger-jsdoc**: Used for generating Swagger documentation. This library generates Swagger API documentation from comments in the code. This allows developers to easily and quickly document their API.
* **Swagger-ui-express**: Used for serving Swagger UI static files. This library allows developers to easily serve Swagger UI static files. This allows developers to show their API documentation in an interactive way.
* **Ts-node**: Used for running TypeScript files directly from the command line. This tool allows developers to compile and run TypeScript files without having to compile them first.
* **Jest**: Used for Testing. This library is a testing framework for JavaScript and TypeScript that offers great test coverage, a simple interface, and a large community.
* **Nodemon**: Used for automatically restarting the server when code is modified. This tool automatically detects code changes and restarts the server to reflect the changes. This saves time for developers by not having to manually restart the server.
* **Superset**: Used for data visualization. This open-source application provides an easy way to create interactive data visualizations. This allows developers to easily explore their data and create reports quickly.
* **Ts-jest**: Used for running TypeScript tests with Jest. This library allows developers to easily run TypeScript tests with Jest. This allows developers to save time by not having to setup a complex testing environment.
* **Jsonwebtoken**: Used for generating tokens for security. This tool allows developers to generate secure tokens for authenticating and authorizing application users.
* **Winston**: Used for logging. This library is a versatile logging tool that allows developers to easily log and manage application events. It provides various features such as log levels, log transports, and customizable formats, making it suitable for different logging requirements.

To manage the lambda functions AWS SAM (Serverless Application Model) has been used which is an open-source framework provided by AWS for building serverless applications. It simplify the deployment and management of serverless resources. With SAM, developers can define serverless applications using a simplified YAML or JSON syntax, and it offers local development and testing capabilities. 

## Development versions 

* Node.js v16.14.0
* npm -v 8.3.1

## Installation

Clone the repository and install the dependencies:

```
👉 git clone https://gitlab.com/agp_portfolio/image_processing.git
👉 cd image_processing
👉 mkdir postgres\data
```


To install the dependencies of the microservice run:

```
👉 cd .\tasks_service
👉 npm install
```

A postgres database is needed for the application to work properly to test it locally.
* (You have postgres installed): Create a database called "tasks". Once you have the database created, you can run the sql to create the table. To do this, the info is in the postgres/init folder. 

You need to create the lambda function in your aws account and change the necesary data in the .env file lo make it work.
If you dont want to use the serverless function, in the branch features/docker you can make it work with an integrated funtion. Due to incompatibilities between the library sharp and some OS use the python lambda function or read the features/docker Instalation_fixes.md


### Microservice command 

First of all you have to be in the tasks_service directory.
Make sure you have the right AWS credentials and function name in the .env

To start the application in development mode:

```
npm run dev
```

To start the application in production mode:

```
npm start
```

To build the application:

```
npm run build
```

To run the tests:

```
npm run test
```

## Dockerized

First of all you have to be in the image_processing directory.
You need to have installed the libraries in the tasks_service.
Make sure you have the right AWS credentials and function name in the .env
If you have Docker installed, you can run the service dockerized:

```
👉 docker-compose up
```

This docker-compose file will create container with a reverse proxy (nginx), a database (postgres), a secure network an the microservice.
You shuld have the lambda fucntion working and configured

### Documentation

The application is documented using Swagger. To access the documentation, open `http://localhost:3000/docs` in your browser.

### Database administration

In the database the persistent data can be checked.
The login info should be for a PostgreSQL:
    * Server: "postgres"
    * User: "postgres"
    * Password: "postgres"
    * Database: "tasks"
