import sharp from "sharp";
import { createHash } from "crypto";

export const process = async (originalImageBuffer, originalImageFilename) => {
  try {
    const filename = String(originalImageFilename);
    const imageName = filename.split(".")[0];
    const imageExt = filename.split(".")[1];

    const resized1024ImageBuffer = await sharp(Buffer.from(originalImageBuffer, 'base64'))
      .resize(1024)
      .toBuffer();

    const resized800ImageBuffer = await sharp(Buffer.from(originalImageBuffer, 'base64'))
      .resize(800)
      .toBuffer();

    const originalMetadata = await sharp(Buffer.from(originalImageBuffer, 'base64')).metadata();
    const originalImageData = {
      width: originalMetadata.width,
      heigth: originalMetadata.height,
      md5Hash: createHash("md5").update(Buffer.from(originalImageBuffer, 'base64')).digest("hex"),
      name: imageName,
      ext: imageExt,
      isOriginal: true,
    };
    const resized1024Metadata = await sharp(resized1024ImageBuffer).metadata();

    const resized1024ImageData = {
      width: resized1024Metadata.width,
      heigth: resized1024Metadata.height,
      md5Hash: createHash("md5").update(resized1024ImageBuffer).digest("hex"),
      name: imageName,
      ext: imageExt,
      isOriginal: false,
      buffer: resized1024ImageBuffer,
    };

    const resized800Metadata = await sharp(resized800ImageBuffer).metadata();

    const resized800ImageData = {
      width: resized800Metadata.width,
      heigth: resized800Metadata.height,
      md5Hash: createHash("md5").update(resized800ImageBuffer).digest("hex"),
      name: imageName,
      ext: imageExt,
      isOriginal: false,
      buffer: resized800ImageBuffer,
    };
    const images = {
      originalImageData,
      resized1024ImageData,
      resized800ImageData,
    };

    return images;
  } catch (error) {
    throw new Error(error);
  }
};
